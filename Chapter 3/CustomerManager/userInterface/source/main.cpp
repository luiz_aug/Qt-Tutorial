#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickView>
#include <controllers/master-controller.h>
#include <controllers/navigation-controller.h>

int main(int argc, char *argv[])
{
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    qmlRegisterType<CustumerManager::controllers::MasterController>("CM", 1, 0,"MasterController");
    qmlRegisterType<CustomerManager::controllers::NavigationController>("CM", 1, 0,"NavigationController");

    CustumerManager::controllers::MasterController masterController;

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("masterController",&masterController);

    const QUrl url(QStringLiteral("qrc:/views/MasterView.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
                        if (!obj && url == objUrl)
                        QCoreApplication::exit(-1);
                        }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
