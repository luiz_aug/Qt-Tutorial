import QtQuick 2.12

Item{
    Rectangle {
        anchors.fill: parent
        color: "#f4c842"

        Text {
            anchors.centerIn: parent
            text: "SplashView"
            font.pixelSize: 48
        }
    }
    Component.onCompleted: contentFrame.replace("qrc:/views/DashboardView.qml");
}
