import QtQuick 2.12

Item{
    Rectangle {
        anchors.fill: parent
        color: "#f4c842"

        Text {
            anchors.centerIn: parent
            text: "FindClientView"
            font.pixelSize: 48
        }
    }
}
