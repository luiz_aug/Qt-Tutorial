import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.VirtualKeyboard 2.5
import QtQuick.Controls 2.12
import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    id: window
    width: 640
    height: 480
    visible: true
    title: qsTr("Client Management")

    StackView {
        id: contentFrame
        anchors {
            top: parent.top
            bottom: parent.bottom
            left:navigationBar.right
            right: parent.right
        }

        initialItem: "qrc:/views/SplashView.qml"
    }
    Rectangle {
        id: navigationBar
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
        }
        width: 100
        color: "#000000"
        Column {
            Button {
                text: "Dashboard"
                onClicked: masterController.ui_NavigationController.goDashboardView()
            }
            Button {
                text: "New Client"
                onClicked: masterController.ui_NavigationController.goCreateClientView()
            }
            Button {
                text: "Find Client"
                onClicked: masterController.ui_NavigationController.goFindClientView()
            }
        }
    }

    Connections {
        target: masterController.ui_NavigationController
        onGoCreateClientView: contentFrame.replace("qrc:/views/CreateClientView.qml")
        onGoDashboardView: contentFrame.replace("qrc:/views/DashboardView.qml")
        onGoEditClientView: contentFrame.replace("qrc:/views/EditClientView.qml", {selectedClient: client})
        onGoFindClientView: contentFrame.replace("qrc:/views/FindClientView.qml")
    }
}
