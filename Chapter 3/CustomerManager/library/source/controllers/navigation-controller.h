#ifndef NAVIGATIONCONTROLLER_H
#define NAVIGATIONCONTROLLER_H
#include <QObject>
#include <library_global.h>
#include <models/client.h>
namespace CustomerManager {
    namespace controllers {
        class LIBRARY_EXPORT NavigationController : public QObject
        {
            Q_OBJECT
            public:
                explicit NavigationController(QObject* _parent = nullptr): QObject(_parent){}
            signals:
                void goCreateClientView();
                void goDashboardView();
                void goEditClientView(CustomerManager::Models::Client* client);
                void goFindClientView();
        };
    }
}

#endif // NAVIGATIONCONTROLLER_H
