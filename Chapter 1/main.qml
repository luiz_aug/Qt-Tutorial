import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    visible: true
    width: 1024
    height: 768
    title: qsTr("ScratchPad")
    color: "#FFFFFF"

    Text {
        id: message
        anchors.centerIn: parent
        font.pixelSize: 44
        text: qsTr("Hello World!")
        color: "#008000"
    }
}
