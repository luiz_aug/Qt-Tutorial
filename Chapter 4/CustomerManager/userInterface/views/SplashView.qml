import QtQuick 2.12
import assets 1.0

Item{
    Rectangle {
        anchors.fill: parent
        color: Style.colourBackground

        Text {
            anchors.centerIn: parent
            text: "SplashView"
            font.pixelSize: 48
        }
    }
    Component.onCompleted: contentFrame.replace("qrc:/views/DashboardView.qml");
}
