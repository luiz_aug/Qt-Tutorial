import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.VirtualKeyboard 2.5
import QtQuick.Controls 2.12
import QtQuick 2.9
import QtQuick.Window 2.2

import assets 1.0
import components 1.0

Window {
    id: window
    width: 640
    height: 480
    visible: true
    title: qsTr("Client Management")

    StackView {
        id: contentFrame
        anchors {
            top: parent.top
            bottom: parent.bottom
            left:navigationBar.right
            right: parent.right
        }

        initialItem: "qrc:/views/SplashView.qml"
    }
    NavigationBar {
        id: navigationBar
    }

    Connections {
        target: masterController.ui_NavigationController
        function onGoCreateClientView(){contentFrame.replace("qrc:/views/CreateClientView.qml")}
        function onGoDashboardView(){contentFrame.replace("qrc:/views/DashboardView.qml")}
        function onGoEditClientView(client){contentFrame.replace("qrc:/views/EditClientView.qml")}
        function onGoFindClientView() {contentFrame.replace("qrc:/views/FindClientView.qml")}
    }
}
