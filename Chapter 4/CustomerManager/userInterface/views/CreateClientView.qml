import QtQuick 2.12
import assets 1.0
import components 1.0

Item{
    Rectangle {
        anchors.fill: parent
        color: Style.colourBackground

        Text {
            anchors.centerIn: parent
            text: "CreateClientView"
            font.pixelSize: 48
        }
    }
    CommandBar {
        commandList: masterController.ui_commandController.ui_createClientViewContextCommands
    }
}
