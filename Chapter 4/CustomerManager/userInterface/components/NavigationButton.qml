import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.VirtualKeyboard 2.5
import QtQuick.Controls 2.12
import QtQuick 2.9
import QtQuick.Window 2.2
import assets 1.0

Item{
    property alias iconCharacter: textIcon.text
    property alias  description: textDescription.text

    property color hoverColour: Style.colourNavigationBarBackground

    signal navigationButtonClicked()

    width: navigationBar.isCollapsed ? Style.widthNavigationBarCollapsed : Style.widthNavigationButton
    height: Style.heightNavigationButton

    states: [
        State {
            name: "hover"
            PropertyChanges {
                target: background
                color: hoverColour

            }
        }]

    Rectangle {
        id: background
        anchors.fill: parent
        color: Style.colourNavigationBarBackground

        Row {
            Text {
                id: textIcon
                width: Style.widthNavigationButtonIcon
                height: Style.heightNavigationButtonIcon
                font {
                    family: Style.fontAwesome
                    pixelSize: Style.pixelSizeNavigationBarIcon
                }
                color: Style.colourNavigationBarFont
                text: "\uf11A"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
            Text {
                id: textDescription
                visible: !navigationBar.isCollapsed
                width:Style.widthNavigationButtonDescription
                height: Style.heightNavigationButtonDescription
                font.pixelSize: Style.pixelSizeNavigationBarText
                color: Style.colourNavigationBarFont
                text: qsTr("SET ME!")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }

        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            hoverEnabled: true
            onEntered: background.state = "hover"
            onExited: background.state = ""
            onClicked: navigationButtonClicked()
        }
    }
}
