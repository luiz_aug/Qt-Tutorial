import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.VirtualKeyboard 2.5
import QtQuick.Controls 2.12

import assets 1.0
import components 1.0

Item {
    property bool isCollapsed : true
    id: navigationBar
    anchors {
        top: parent.top
        left: parent.left
        bottom: parent.bottom
    }
    width: isCollapsed ? Style.widthNavigationBarCollapsed : Style.heightNavigationBarExpanded

    Rectangle {
        anchors.fill: parent
        color: Style.colourNavigationBarBackground

        Column {
            id: navigationColumn
            NavigationButton {
                id: menuButton
                iconCharacter: "\uf0c9"
                description: qsTr("")
                hoverColour: "#993333"
                onNavigationButtonClicked: isCollapsed = !isCollapsed
            }
            NavigationButton {
                id: dashboardButton
                iconCharacter: "\uf015"
                description: qsTr("Dashboard")
                hoverColour: "#993333"
                onNavigationButtonClicked: masterController.ui_NavigationController.goDashboardView()
            }
            NavigationButton {
                id: newClientButton
                iconCharacter: "\uf234"
                description: qsTr("New Client")
                hoverColour: "#993333"
                onNavigationButtonClicked: masterController.ui_NavigationController.goCreateClientView()
            }
            NavigationButton {
                id: findClientButton
                iconCharacter: "\uf002"
                description: qsTr("Find Client")
                hoverColour: "#993333"
                onNavigationButtonClicked: masterController.ui_NavigationController.goFindClientView()
            }
        }
    }
}
