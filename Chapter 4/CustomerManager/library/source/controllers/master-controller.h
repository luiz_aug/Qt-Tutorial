#ifndef MASTERCONTROLLER_H
#define MASTERCONTROLLER_H

#include <QObject>
#include <QString>
#include <QScopedPointer>

#include <library_global.h>
#include <controllers/navigation-controller.h>
#include <controllers/commandcontroller.h>

namespace CustumerManager {
    namespace controllers {
        class LIBRARY_EXPORT MasterController : public QObject
        {
            Q_OBJECT
            Q_PROPERTY( QString ui_welcomeMessage MEMBER welcomeMessage CONSTANT )
            Q_PROPERTY( CustomerManager::controllers::NavigationController* ui_NavigationController MEMBER navigationController CONSTANT )
            Q_PROPERTY( CustomerManager::controllers::CommandController* ui_commandController MEMBER commandController CONSTANT )

            public:
                explicit MasterController (QObject* parent = nullptr);
                //~MasterController();
                QString welcomeMessage = "This is MasterController to Major Tom";
                CustomerManager::controllers::NavigationController* navigationController;
                CustomerManager::controllers::CommandController* commandController;
            private:
        };

    }
}

#endif // MASTERCONTROLLER_H
