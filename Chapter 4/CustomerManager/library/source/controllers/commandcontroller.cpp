#include "commandcontroller.h"
#include <QList>
#include <QDebug>

namespace CustomerManager {
namespace controllers {
    CommandController::CommandController(QObject *parent) : QObject(parent)
    {
        CustomerManager::framework::Command* createClientSaveCommand =
                        new CustomerManager::framework::Command(parent, QChar( 0xf0c7 ), "Save", true);
        QObject::connect(createClientSaveCommand, &CustomerManager::framework::Command::executed,
                        this, &CommandController::onCreateClientSaveExecuted);
        this->createClientViewContextCommands.append( createClientSaveCommand );
    }

    CommandController::~CommandController()
    {
    }

    QQmlListProperty<CustomerManager::framework::Command> CommandController::ui_createClientViewContextCommands()
    {
        return QQmlListProperty<CustomerManager::framework::Command>(this,&createClientViewContextCommands);
    }

    void CommandController::onCreateClientSaveExecuted()
    {
    qDebug() << "You executed the Save command!";
    }
}
}
