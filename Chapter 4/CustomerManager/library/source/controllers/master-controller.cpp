#include "master-controller.h"
#include "navigation-controller.h"

namespace CustumerManager {
    namespace controllers {
    MasterController::MasterController(QObject* parent) : QObject(parent)
    {
        this->navigationController = new CustomerManager::controllers::NavigationController(this);
        this->commandController = new CustomerManager::controllers::CommandController(this);
    }

}
}
