#ifndef COMMANDCONTROLLER_H
#define COMMANDCONTROLLER_H

#include <QObject>
#include <QtQml/QQmlListProperty>
#include <library_global.h>
#include <framework/command.h>

namespace CustomerManager {
namespace controllers {
    class CommandController : public QObject
    {
        Q_OBJECT
        Q_PROPERTY(QQmlListProperty<CustomerManager::framework::Command> ui_createClientViewContextCommands READ ui_createClientViewContextCommands CONSTANT)
        public:
            explicit CommandController(QObject *parent = nullptr);
            ~CommandController();
            QQmlListProperty<framework::Command> ui_createClientViewContextCommands();
            //CustomerManager::framework::Command* command;
            QList<CustomerManager::framework::Command*> createClientViewContextCommands{};
        public slots:
            void onCreateClientSaveExecuted();

    };
}
}
#endif // COMMANDCONTROLLER_H
