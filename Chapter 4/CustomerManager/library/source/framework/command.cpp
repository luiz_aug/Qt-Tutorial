#include "command.h"

namespace CustomerManager {
namespace framework {
    Command::Command(QObject* parent, const QString& iconCharacter, const
                     QString& description, bool canExecute) : QObject(parent)
    {
        this->iconCharacter = iconCharacter;
        this->description = description;
        this->canExecute = canExecute;
    }

    Command::Command(QObject* parent):QObject(parent){}
    Command::~Command()
    {
    }
}
}

