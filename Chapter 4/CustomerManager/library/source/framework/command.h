#ifndef COMMAND_H
#define COMMAND_H

#include <QObject>

#include <functional>
#include <QScopedPointer>
#include <QString>

#include <library_global.h>

namespace CustomerManager {
    namespace framework {
        class LIBRARY_EXPORT Command : public QObject
        {
            Q_OBJECT
            Q_PROPERTY( QString ui_iconCharacter MEMBER iconCharacter CONSTANT )
            Q_PROPERTY( QString ui_description MEMBER description CONSTANT )
            Q_PROPERTY( bool ui_canExecute MEMBER canExecute NOTIFY canExecuteChanged)

            public:
                explicit Command(QObject* parent, const QString& iconCharacter, const
                                 QString& description, bool canExecute);
                explicit Command(QObject* parent=nullptr);
                ~Command();
                QString iconCharacter;
                QString description;
                bool canExecute;

            signals:
                void canExecuteChanged();
                void executed();

            private:

        };
}
}
#endif // COMMAND_H
