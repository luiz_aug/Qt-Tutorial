import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.VirtualKeyboard 2.4

Window {
    id: window
    width: 640
    height: 480
    visible: true
    title: qsTr("Client Management")

    Text {
        text: masterController.ui_welcomeMessage
    }
}
